import {
  ConflictException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from "@nestjs/common";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { UserRepository } from "./user.repository";
import { User } from "./entities/user.entity";
import { PokemonService } from "src/pokemon/pokemon.service";
import { RolesRepository } from "src/roles/roles.repository";
import { Role } from "src/roles/entities/role.entity";

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly pokemonService: PokemonService,
    private readonly rolesRepository: RolesRepository
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const userExists = await this.userRepository.findByUsername(
      createUserDto.username
    );
    if (userExists) {
      throw new ConflictException("User Exists");
    }
    if (!createUserDto.roles) {
      throw new ConflictException("User must have at least one role");
    }
    const roles = await this.rolesRepository.findAll();
    for (let i = 0; i < createUserDto.roles.length; i++) {
      const role = roles.find((role) => role.id === createUserDto.roles[i].id);
      if (!role) {
        throw new NotFoundException(
          `Role with id "${createUserDto.roles[i].id}" does not exist`
        );
      }
    }
    // get all pokemons
    const pokemons = await this.pokemonService.getPokemons();
    // get a random pokemon
    const pokemon = pokemons[Math.floor(Math.random() * pokemons.length)];
    const user = await this.userRepository.create({
      ...createUserDto,
      pokemon,
    });
    return user;
  }

  async validate(username: string, password: string): Promise<User> {
    const userExists = await this.userRepository.findByUsername(username);
    if (!userExists || userExists.password !== password) {
      throw new UnauthorizedException(
        "Invalid username or password. Please try again."
      );
    }
    return userExists;
  }

  getUserRoles = async (userRoles: number[] = []) => {
    const roles = await this.rolesRepository.findAll();
    return roles
      .filter((role) => userRoles.includes(role.id))
      .map((role) => role.name);
  };

  async findOne(id: number) {
    const user = await this.userRepository.findById(id);
    if (!user) {
      throw new NotFoundException("User not found");
    }
    return user;
  }

  async findAll() {
    return await this.userRepository.list();
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.userRepository.findById(id);
    if (!user) {
      throw new Error("User not found");
    }
    return this.userRepository.update(user, updateUserDto);
  }

  private getUserRoleIds(roles: Role[] = []): number[] {
    return roles.map((role) => role.id);
  }

  async delete(id: number) {
    return await this.userRepository.delete(id);
  }

  async getPokemons() {
    return await this.pokemonService.getPokemons();
  }
}
