import { User } from "./entities/user.entity";
import { DataSource } from "typeorm";
import { CreateUserDto } from "./dto/create-user.dto";
import { Injectable } from "@nestjs/common";
import { UpdateUserDto } from "./dto/update-user.dto";

@Injectable()
export class UserRepository {
  constructor(private dataSource: DataSource) {}

  async create(createUserDto: CreateUserDto) {
    return await this.dataSource.getRepository(User).save(createUserDto);
  }

  async findById(id: number) {
    const user = this.dataSource
      .createQueryBuilder(User, "user")
      .leftJoinAndSelect("user.roles", "roles")
      .where("user.id = :id", { id })
      .getOne();
    return user;
  }

  async findByUsername(username: string): Promise<User> {
    const user = await this.dataSource.manager
      .createQueryBuilder(User, "user")
      .leftJoinAndSelect("user.roles", "roles")
      .where("user.username = :username", { username })
      .getOne();
    return user;
  }

  async list() {
    const users = await this.dataSource
      .getRepository(User)
      .createQueryBuilder("user")
      .leftJoinAndSelect("user.roles", "roles")
      .getMany();
    return users;
  }

  async update(user: User, updateUserDto: UpdateUserDto) {
    return this.dataSource.getRepository(User).save({
      ...user,
      ...updateUserDto,
    });
  }

  async delete(id: number) {
    await this.dataSource
      .createQueryBuilder()
      .delete()
      .from(User)
      .where("id = :id", { id })
      .execute();
  }
}
