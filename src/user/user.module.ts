import { Module } from "@nestjs/common";
import { UserService } from "./user.service";
import { UserController } from "./user.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "./entities/user.entity";
import { UserRepository } from "./user.repository";
import { PokemonModule } from "src/pokemon/pokemon.module";
import { RolesRepository } from "src/roles/roles.repository";
import { Role } from "src/roles/entities/role.entity";
import { RolesService } from "src/roles/roles.service";

@Module({
  imports: [TypeOrmModule.forFeature([User, Role]), PokemonModule],
  controllers: [UserController],
  providers: [UserService, UserRepository, RolesService, RolesRepository],
})
export class UserModule {}
