import { PartialType } from "@nestjs/mapped-types";
import { CreateUserDto } from "./create-user.dto";
import {
  IsAlphanumeric,
  IsEmail,
  IsOptional,
  IsString,
  MinLength,
} from "class-validator";

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @IsString()
  @IsOptional()
  @MinLength(5, { message: "Name must have at least 5 characters" })
  name?: string;

  @IsString()
  @IsOptional()
  @MinLength(3, {
    message: "Username must have at least 3 characters",
  })
  @IsAlphanumeric(null, {
    message: "Username must have only letters and numbers",
  })
  username?: string;

  @IsString()
  @IsOptional()
  @IsEmail(null, { message: "Invalid email" })
  email?: string;

  @IsString()
  @IsOptional()
  password?: string;

  @IsString()
  @IsOptional()
  pokemon?: string;

  @IsString()
  @IsOptional()
  role?: string;
}
