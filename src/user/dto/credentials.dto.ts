import { IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class CredentialsDto {
  @IsString()
  @IsNotEmpty()
  @IsEmail()
  username: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(6, { message: "Password must have at least 6 characters" })
  password: string;
}
