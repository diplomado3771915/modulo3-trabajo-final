import { Module } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { AuthController } from "./auth.controller";
import { User } from "src/user/entities/user.entity";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserModule } from "src/user/user.module";
import { JwtModule } from "@nestjs/jwt";
import { UserService } from "src/user/user.service";
import { UserRepository } from "src/user/user.repository";
import { jwtConstants } from "src/constants/constant";
import { JwtStrategy } from "./jwt.strategy";
import { PokemonModule } from "src/pokemon/pokemon.module";
import { RolesModule } from "src/roles/roles.module";
import { RolesService } from "src/roles/roles.service";
import { RolesRepository } from "src/roles/roles.repository";

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    UserModule,
    PokemonModule,
    RolesModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: "3600s" },
    }),
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    UserService,
    UserRepository,
    JwtStrategy,
    RolesService,
    RolesRepository,
  ],
})
export class AuthModule {}
