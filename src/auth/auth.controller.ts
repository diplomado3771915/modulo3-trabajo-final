import { Body, Controller, Post } from "@nestjs/common";
import { CredentialsDto } from "src/user/dto/credentials.dto";
import { UserService } from "src/user/user.service";
import { AuthService } from "./auth.service";
import { ApiTags } from "@nestjs/swagger";

@ApiTags("auth")
@Controller("auth")
export class AuthController {
  constructor(
    private userService: UserService,
    private authService: AuthService
  ) {}

  @Post("login")
  async login(@Body() credentialsDTO: CredentialsDto) {
    const user = await this.userService.validate(
      credentialsDTO.username,
      credentialsDTO.password
    );
    return this.authService.login(user);
  }
}
