import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { User } from "src/user/entities/user.entity";

@Injectable()
export class JwtUtil {
  constructor(private readonly jwtService: JwtService) {}

  decode(auth: string): User {
    const jwt = auth.replace("Bearer ", "");
    return this.jwtService.decode(jwt, { json: true }) as User;
  }
}
