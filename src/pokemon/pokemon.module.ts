import { Module } from "@nestjs/common";
import { PokemonService } from "./pokemon.service";
import { PokemonController } from './pokemon.controller';

@Module({
  imports: [],
  providers: [PokemonService],
  exports: [PokemonService],
  controllers: [PokemonController],
})
export class PokemonModule {}
