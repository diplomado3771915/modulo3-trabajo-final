import { Injectable } from "@nestjs/common";
import axios from "axios";

@Injectable()
export class PokemonService {
  private readonly apiUrl = "https://pokeapi.co/api/v2";

  async getPokemons() {
    try {
      const res = await axios.get(`${this.apiUrl}/pokemon?limit=151`);
      const pokemons = res.data;
      return pokemons.results.map((pokemon: { name: string }) => pokemon.name);
    } catch (error) {
      throw new Error("Error fetching pokemons");
    }
  }
}
