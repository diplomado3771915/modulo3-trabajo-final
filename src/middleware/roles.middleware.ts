import { Injectable, NestMiddleware } from "@nestjs/common";
import { JwtUtil } from "src/auth/jwt.util";
import { RolesService } from "src/roles/roles.service";

@Injectable()
export class RolesMiddleware implements NestMiddleware {
  constructor(
    private readonly rolesService: RolesService,
    private readonly jwtUtil: JwtUtil
  ) {}
  use(req: any, res: any, next: () => void) {
    this.rolesService.findByName("admin").then((adminRole) => {
      const auth = req.headers.authorization;
      const userRoles = this.jwtUtil.decode(auth).roles;
      if (userRoles.find((role) => role.id === adminRole.id)) {
        console.log("User has admin role");
        next();
      } else {
        console.log("User does not have admin role");
        res.status(403).send("Forbidden");
      }
    });
  }
}
