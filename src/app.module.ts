import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserModule } from "./user/user.module";
import { User } from "./user/entities/user.entity";
import { LoggerMiddleware } from "./middleware/logger.middleware";
import { AuthModule } from "./auth/auth.module";
import { PokemonModule } from "./pokemon/pokemon.module";
import { PokemonController } from "./pokemon/pokemon.controller";
import { RolesModule } from "./roles/roles.module";
import { Role } from "./roles/entities/role.entity";
import { ConfigModule } from "@nestjs/config";
import { RolesService } from "./roles/roles.service";
import { RolesRepository } from "./roles/roles.repository";
import { RolesMiddleware } from "./middleware/roles.middleware";
import { JwtUtil } from "./auth/jwt.util";
import { JwtService } from "@nestjs/jwt";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: "postgres",
      host: process.env.DATABASE_HOST,
      port: +process.env.DATABASE_PORT,
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      entities: [User, Role],
      synchronize: true,
      logging: true,
    }),
    UserModule,
    AuthModule,
    PokemonModule,
    RolesModule,
  ],
  controllers: [AppController, PokemonController],
  providers: [AppService, RolesService, RolesRepository, JwtUtil, JwtService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes("*")
      .apply(RolesMiddleware)
      .forRoutes("user");
    // .apply(
    //   (req, res, next) => {
    //     console.log("Using forRoutes({path, method})");
    //     console.log("syncronous middleware");
    //     next();
    //   },
    //   async (req, res, next) => {
    //     console.log("Using forRoutes({path, method})");
    //     console.log("asyncronous middleware");
    //     // const start = Date.now();
    //     // const done = await asyncTimeout(5000);
    //     // console.log(done);
    //     // console.log('Time taken:' + (Date.now() - start));
    //     next();
    //   }
    // )
    // .forRoutes({ path: "user", method: RequestMethod.GET });
  }
}
