import { ApiProperty, PartialType } from "@nestjs/swagger";
import { CreateRoleDto } from "./create-role.dto";
import { IsNotEmpty, IsOptional, IsString } from "class-validator";

export class UpdateRoleDto extends PartialType(CreateRoleDto) {
  @ApiProperty({
    description: "Role name",
    example: "admin",
    required: true,
  })
  @IsString()
  @IsOptional()
  name?: string;
}
