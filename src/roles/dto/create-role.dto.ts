import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class CreateRoleDto {
  @ApiProperty({
    description: "Role name",
    example: "admin",
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  name: string;
}
