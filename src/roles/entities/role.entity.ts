import { User } from "src/user/entities/user.entity";
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Role {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "varchar", length: 30, unique: true })
  name: string;

  @ManyToMany(() => User, (user) => user.roles)
  users: User[];

  constructor(partial?: Partial<Role>) {
    if (partial) {
      Object.assign(this, partial);
    }
  }
}
