import { Injectable } from "@nestjs/common";
import { DataSource } from "typeorm";
import { CreateRoleDto } from "./dto/create-role.dto";
import { Role } from "./entities/role.entity";
import { UpdateRoleDto } from "./dto/update-role.dto";

@Injectable()
export class RolesRepository {
  constructor(private dataSource: DataSource) {}

  async create(role: CreateRoleDto) {
    return await this.dataSource.getRepository(Role).save(role);
  }

  async findAll() {
    const roles = await this.dataSource
      .getRepository(Role)
      .createQueryBuilder("role")
      .getMany();
    return roles;
  }

  async findById(id: number) {
    const role = await this.dataSource
      .getRepository(Role)
      .createQueryBuilder("role")
      .where("role.id = :id", { id })
      .getOne();
    return role;
  }

  async findByName(name: string) {
    const role = await this.dataSource
      .getRepository(Role)
      .createQueryBuilder("role")
      .where("role.name = :name", { name })
      .getOne();
    return role;
  }

  async update(id: number, role: UpdateRoleDto) {
    await this.dataSource
      .createQueryBuilder()
      .update(Role)
      .set({ ...role })
      .where("id = :id", { id })
      .execute();
  }

  async delete(id: number) {
    await this.dataSource
      .createQueryBuilder()
      .delete()
      .from(Role)
      .where("id = :id", { id })
      .execute();
  }
}
