import { ConflictException, Injectable } from "@nestjs/common";
import { CreateRoleDto } from "./dto/create-role.dto";
import { UpdateRoleDto } from "./dto/update-role.dto";
import { RolesRepository } from "./roles.repository";

@Injectable()
export class RolesService {
  constructor(private readonly rolesRepository: RolesRepository) {}

  async create(createRoleDto: CreateRoleDto) {
    const roleExists = await this.rolesRepository.findByName(
      createRoleDto.name
    );
    if (roleExists) {
      throw new ConflictException("Role already exists");
    }
    return await this.rolesRepository.create(createRoleDto);
  }

  async findAll() {
    return await this.rolesRepository.findAll();
  }

  findOne(id: number) {
    return this.rolesRepository.findById(id);
  }

  findByName(name: string) {
    return this.rolesRepository.findByName(name);
  }

  update(id: number, updateRoleDto: UpdateRoleDto) {
    return this.rolesRepository.update(id, updateRoleDto);
  }

  remove(id: number) {
    return this.rolesRepository.delete(id);
  }
}
